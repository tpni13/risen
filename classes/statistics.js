var request = require('request');
var csvparse = require('csv-parse');

function Statistics(cache)
{
	this.cache = cache;
	this.options = {headers: {'User-Agent': 'Felony'}};

	// Create zipcodes instance
	var CriminalStatistics = require('./criminalstatistics.js');
	this.criminalstatistics = new CriminalStatistics(this.cache, this.options, request, csvparse);
}



Statistics.prototype.cacheCriminalStatistics = function(callback)
{
	var that = this;
	this.criminalstatistics.fetchTableInfo(function()
	{
		that.criminalstatistics.fetchData(callback);
	});
}

Statistics.prototype.fetchCriminalStatistics = function(callback, area)
{
	var that = this;
	this.criminalstatistics.fetchTableInfo(function()
	{
		that.criminalstatistics.fetchData(callback);
	});
}



module.exports = Statistics;