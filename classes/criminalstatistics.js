function CriminalStatistics(cache, options, request, csvparse)
{
	this.cache = cache;
	this.options = options;
	this.request = request;
	this.csvparse = csvparse;
}



CriminalStatistics.prototype.fetchTableInfo = function(callback)
{
	var that = this;

	that.request('http://api.statbank.dk/v1/tableinfo/STRAF11?format=JSON', that.options, function(error, response, result)
	{
		if((response.statusCode == 200 ? true : false))
		{
			result = JSON.parse(result).variables;
			result.map(function(data)
			{
				if(data.id == 'OMRÅDE')
				{
					data.values.map(function(item)
					{
						// Cache people
						that.cache.set('area_' + parseInt(item.id), item.text);
					});
				}

				if(data.id == 'OVERTRÆD')
				{
					data.values.map(function(item)
					{
						// Cache people
						that.cache.set('felony_' + parseInt(item.id), item.text);
					});
				}
			});

			// E.T phone home when done caching
			callback();
		}
	});
}

CriminalStatistics.prototype.fetchData = function(callback)
{
	var that = this;
	var statistics = {};

	// http://api.statbank.dk/v1/data/STRAF11/CSV?valuePresentation=Code&OMR%C3%85DE=*&OVERTR%C3%86D=*&Tid=2015K4
	that.request('http://127.0.0.1:' + process.env.PORT + '/straf11.csv', that.options, function(error, response, result)
	{
		if((response.statusCode == 200 ? true : false))
		{
			that.csvparse(result, {delimiter: ';', skip_empty_lines: true}, function(err, output)
			{
				if(!err)
				{
					output.map(function(item, index)
					{
						if(index > 0)
						{
							var area = parseInt(item[0]), time = item[2], felony = item[1];

							if(area !== null)
							{

								/*if(statistics[area] === undefined)
									statistics[area] = {};

								if(statistics[area][time] === undefined)
									statistics[area][time] = {};

								//statistics[area][time][felony] = item[3];*/

								if(statistics[area] === undefined || statistics[area] === null)
									statistics[area] = {id: parseInt(area)};

								if(statistics[area][time] === undefined || statistics[area][time] === null)
									statistics[area][time] = {};

								if(statistics[area][time][felony] === undefined || statistics[area][time][felony] === null)
									statistics[area][time][felony] = {id: parseInt(felony), value: parseInt(item[3])};

							}
						}
					});

					// Cache people
					//that.cache.set('cs_' + parseInt(item[0]), item[3]);

					// E.T phone home when done caching
					callback(statistics);
				}
			});
		}
	});
}



module.exports = CriminalStatistics;