var express = require('express');
var app = express();

if(process.env.PORT == undefined)
    process.env.PORT = 5000;

app.set('port', process.env.PORT);



// Cache
var NodeCache = require("node-cache");
var myCache = new NodeCache({stdTTL: 0, checkperiod: 0});

// Create zipcodes instance
var Statistics = require('./classes/statistics.js');
var statistics = new Statistics(myCache);
var request = require('request');

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');



app.get('/', function(req, res)
{
    res.render('pages/index');
});

app.get('/felony(\/[0-9]+)?', function(req, res)
{
    statistics.fetchCriminalStatistics(function(data)
    {
        res.send(data);
    }, parseInt(req.params[0]));
});

app.get('/rest/soeg/:string', function(req, res)
{
    var queryString = 'https://www.tinglysning.dk/rest/soeg/' + encodeURIComponent(req.params.string);
    
    request(queryString, {headers: {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'}}, function(error, requestResponse, result)
    {
        if((requestResponse.statusCode == 200 ? true : false))
        {
            res.send(result);
        }
    });
});

app.get('/rest/ejendom/:string', function(req, res)
{
    request('https://www.tinglysning.dk/rest/ejendom/' + req.params.string, {headers: {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36'}}, function(error, requestResponse, result)
    {
        if((requestResponse.statusCode == 200 ? true : false))
        {
            res.send(result);
        }
    });
});

var server = app.listen(app.get('port'), function()
{
    console.log('Node app is running on port', app.get('port'));

    setTimeout(function () {
            server.close();
            // ^^^^^^^^^^^
        }, 3000)
});